%!TEX encoding = IsoLatin
\documentclass[11pt]{letter}

\usepackage[left=4cm,top=2cm,right=3cm,bottom=3cm]{geometry}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{times}
\usepackage{url}
\usepackage[ansinew]{inputenc}
\usepackage[cyr]{aeguill}
\usepackage{wasysym}
%\usepackage[french]{babel}

\usepackage{color}


\begin{document}

\pagestyle{fancy}
\renewcommand{\headrulewidth}{0em}
\vspace{0.25cm}
\lfoot{}
\cfoot{--\ \thepage\ --}

\textbf{\LARGE Answers to comments $\#$1}\\

\begin{bfseries}
Title: Comparison with structurally-sparse DNNs using group Lasso

Comment: The recently proposed structurally-sparse DNNs based on group Lasso can ''simultaneously'' learn sparse block structures (e.g. sparse filters) and minimize the classification error:

(1) http://papers.nips.cc/paper/6504-learning-structured-sparsity-in-deep-neural-networks.pdf\\
(2) http://papers.nips.cc/paper/6372-learning-the-number-of-neurons-in-deep-networks.pdf

Unlike those works, the paper uses a separate scheme to optimize the sparse blocks and classification error. To prove the effectiveness of the proposed method, It would be more comprehensive to compare it with methods based on group Lasso regularization.
\end{bfseries}

Thank you for these highly relevant references. These were published after we submitted the paper. We are citing both of these references in the latest revision of our paper, they are interesting works on sparse CNNs. Compared to the closely related Sparse Structure Learning (SSL) method from Wen et al. (2016), differentiability and separability are the major benefits obtained by adopting the ADMM framework. These two properties are highlighted in the claims 1 and 2 in the Discussion section in the latest version of the paper. See also in the Discussion section the text starting with "Some methods such as SSL...", up to the end of the section, which clarifies the distinction between our proposed method and the SSL method from Wen et al. (2016).


\vspace{1em}
\textbf{\LARGE Answers to comments $\#$2}\\

\begin{bfseries}
Could the authors elaborate more on point (3) in their conclusion?  Like Reviewer3, I'd like some more intuition as to how this particular point is supposed to be compared against  the more standard L-1 regularization, which is likewise differentiable.  Is the claim that this algorithm is sort of like a differentiable version of L-0 regularization?

Also, could the authors speculate more about why some of the models become better with more sparsity?  Are there enough experiments to determine if this is just a statistical fluke, or is this more of a general observation?  Or perhaps this is just an artifact of the sparsity allowing for longer training times?  Numerics would be nice here, but really I'm just curious.

Review: 
The paper presents a method for sparsifying the weights of the convolutional filters and fully-connected layers of a CNN without loss of performance. Sparsification is achieved by using augmenting the CNN objective function with a regularization term promoting sparisty on groups of weights. The authors use ADMM for solving this optimization task, which allows decoupling of the two terms. The method alternates between promoting the sparsity of the network and optimizing the recognition performance

The method is technically sound and clearly explained. The paper is well organised and the ideas presented in a structured manner. I believe that sometimes the wording could be improved.

The proposed method is simple and effective. Using it in combination with other CNN compression techniques such as quantization/encoding is a promising direction for future research. The experimental evaluation is convincing in the sense that the method seems to work well. The authors do not use state-of-the-art CNNs architectures, but I don't see this a requirement to deliver the message. 

On the other hand, the proposed method is closely related to recent works (as shown in the references posted in the public comment titled "Comparison with structurally-sparse DNNs using group Lasso), that should be cited and compared against (at least at a conceptual level). I will of course consider the authors rebuttal on this matter.

It would be very important for the authors to comment on the differences between these works and the proposed approach. It seems that both of these references use sparsity regularized training for neural networks, with a very similar formulation.

The authors choose to optimize the proposed objective function using ADMM. It is not clear to me, why this approach should be more effective than proximal gradient descent methods. Could you please elaborate on this? ADMM is more demanding in terms of memory (2 copies of the parameters need to be stored). 

The claimed contributions (Section 5) seem a bit misleading in my opinion. Using sparsity promoting regularization in the parameters of the model has been used by many works, in particular in the dictionary learning literature but also in the neural network community (as stated above).  Claims 1,2 and 3, are well understood properties of L1-type regularizers. As written in the current version of the manuscript, it seems that these are claimed contributions of this particular work. 

A discussion on why sparsity sometimes helps improve performance could be interesting. 

In the experimental section, the authors mainly concentrate on comparing accuracy vs parameter reduction. While this is naturally very relevant property to report, it would be also interesting to show more results in terms of speed-up. which should also be improved by the proposed approach.

Other minor issues:
- In (3): I think a $\wedge2$ is missing in the augmented term (the rightmost term).

- The authors could cite the approach by Han et all for compressing DNNS:
Han, ICLR 2016 "Deep compression: Compressing deep neural network with pruning, trained quantization and huffman coding"
\end{bfseries}

Thank you for your careful and thoughtful review, that's really appreciated.

[Elaborate more on point (3) of the conclusion]: Indeed, the ADMM method breaks the minimization of the augmented Lagrangian function into two parts, which brings the differentiability advantage to the proposed method for loss minimization, while making possible the use of L0 regularization.

[Statistical significance of generalization improvements with more sparsity]: In order to demonstrate that the ADMM training performance is statistically significant, we did a t-test by repeating the experiment 15 times on CIFAR-10 using the NIN model. The results demonstrate that ADMM training achieves significant improvements from both the baseline model (t-test result with p<0.001) and standard fine tuning (t-test result with p<0.001). The t-test results are given in the new appendix A. 

[Comparison with group lasso work]: Compared to the closely related Sparse Structure Learning (SSL) method, which learns sparse block structures (e.g. sparse filters) and minimizes the classification error simultaneously, our proposed approach uses ADMM method to provide a separate scheme to optimize the sparse blocks and classification error. ADMM brings the differentiability and separability advantages to the proposed sparse CNN method which are the basis of the distinctive contributions of our proposed approach compared to SSL method. The algorithm has the advantage that it is partially and analytically solvable due to the separability property. This contributes to the efficient trainability of the model. Moreover, the differentiability problem of L0-norm penalty function makes it unusable in the SSL framework, while L0-norm can be incorporated as a mean of sparsity penalty terms in our proposed method.

[Effectiveness of ADMM]: The ADMM method breaks the minimization of the augmented Lagrangian function into two parts which brings the differentiability and separability advantages to the proposed method (claims 1 and 2 in the Discussion section). Although during the training phase ADMM seems to be more demanding in terms of memory (2 copies of the parameters need to be stored), after the convergence only one copy of parameters with sparse structure is retrieved and used in the test phase and the final model benefits from a more compact size (smaller memory footprint).

[Confusion over claimed contributions]: We agree with the reviewer's comment, implying that the statement 1, 2, and 3 may induce some confusion to the readers. Actually, compared against the standard L1 regularization, which is somehow differentiable, the major point is that due to the decoupling property of ADMM, the proposed algorithm makes it possible to use the non-differentiable L0 regularization (differentiability advantage). Furthermore, the decoupling capability of ADMM algorithm along with the separability of the penalty functions provide analytical solutions to the sparsity-promoting problems (separability advantage). In order to clarify this, we revised the statement in the discussion part to elucidate the notions of the /differentiability/ and the /separability/ of the proposed method. 

[Discussion on why sparsity helps improve performance]: The advantage of the complicated models is that they can capture highly non-linear relationship between features and output. The drawback of such large models is that they are prone to capture the noise that does not generalize to new datasets, leading to over-fitting and a high variance. The recently presented compression methods of dense networks without losing accuracy show significant redundancy in the trained model and inadequacy of current training methods to find the existing sparse and compact solutions with better generalization properties. We propose ADMM-based training method as a post-processing step, once a good model has been learned, with the solution gradually moving from the original dense network to the sparse structure of interest, as our emphasis on the sparsity-promoting penalty term is increased.

More specifically, since the target solution is likely to be sparse, we may think that enforcing sparsity right in the beginning, using our proposed method, will provide a way to avoid overfitting for achieving a better performance. However, increasing more the sparsity strength of the solution may lead to over-smoothing the models and drops in the performance. Therefore, in practical design of networks, we figured out that this regularization factor should be increased gradually, until the desired balance between performance and sparsity is achieved.

Therefore, from our preliminary experiments, we observed that sparsifying networks gradually and after a good dense network has been learned is better at improving performance. We conjuncture that the learning method may need to have enough room to first explore and find a good position in the search space of possible models, before applying gradual sparsification over it. Otherwise, a strong sparsification penalty may break the smoothness of the search space, making it much harder to explore sparser solutions in the neighborhood of the current model. Verifying this may be the topic of a future work. 

[Report speedup]: We reported speedup as we vary the parameter mu to the results of Tables 3, 4, and 5 in Appendix B. As the reviewer truly noticed, the number of parameters in the network is reduced by the proposed method and in practice, a speedup for all the networks is achieved. 

[Cite Han et al. (2016)]: Thank you for suggesting the interesting and relevant paper by Han et al. (2016), which is cited in the latest revision of our paper.

[Correct Eq. 3]: We also edited the augmented Lagrangian equation (3).

We hope you find the modified paper as a better presentation of the work.


\vspace{1em}

\textbf{\Large Comments from Reviewer \#2}\\

\begin{bfseries}
Review: This paper reduces the computational complexity of CNNs by minimizing the recognition loss and a sparsity-promoting penalty term together. The authors use ADMM, which is widely used in optimization problems but not used with CNNs before.
The paper has a good motivation and well written. The experiments show that the proposed approach increases the sparsity in a network as well as increases the performance.

As the authors stated, ADMM approach is not guaranteed to converge in non-convex problems. The authors used pre-trained networks to mitigate the problem of trapping into a local optimum. However, the datasets that are used are very small. It would be good to investigate how the proposed approach works on bigger datasets such as ImageNet.

Authors should compare their results with previous studies that use pruning or sparsity regularizers (Liu et al. (2015); Han et al. (2015); Collins $\&$ Kohli (2014)).

In the discussion section, authors stated that the proposed approach is efficient in training because of the separability property. Could you elaborate on that? Lets say this work uses two phases; phase 1 is pre-training a network, phase 2 is using sparsity and performance promoting steps.  Phase 2 also includes fine-tuning the network based on the new sparse structure. How long does the phase 2 take compare to phase 1? How many epochs is needed to fine-tune the network?
\end{bfseries}

Thank you for your review.

To answer this point, we added the following text to the revised version of the paper, starting in second paragraph of the Experimental Results section.

--- Begin quoted text ---

Since the regularization factor $\mu$ is selected from gradually increasing values, for the first small values of $\mu$ the selection of long epochs for performance-promoting step (inner loop) and fine-tuning steps is computationally prohibitive and would result in over-fitting. Instead, we start with one epoch for the first $\mu$ and increase the number of epochs by $\delta$ for the next $\mu$ values up to the $\nu$-th $\mu$ value, after which the number of epochs is limited to $\delta\nu$. We found that $\delta=1$ and $\nu=15$ generally work well in our experiments. We already incorporated the number of training epochs at tables 3, 4, and 5 of Appendix B. If the maximum limiting number of iterations of inner loop is $\xi$ (suggested value of $\xi$=10), the training time of the $\nu$-th $\mu$ value takes a total of $\delta\nu\xi+\delta\nu$ epochs ($\delta\nu\xi$ for performance-promoting step and $\delta\nu$ for fine-tuning) under the worst-case assumption, where the inner loop has not converged and completes only at the $\xi$-th iteration.

--- End quoted text ---

We hope this will answer well the questions of your last paragraph.

%These previously-pruned connections are initialized to zero and the entire network is retrained with 1/10 the original learning rate (since the sparse network is already at a good local minima). Hyper parameters like dropout ratios and weight decay remained unchanged. By restoring the pruned connections, the final D step increases the model capacity of the network and make it possible to arrive at a better local minima compared with the sparse model from S step.
%
%The SSL step is essential to learn structured sparsity, and fine-tuning process is the step to recover accuracy a bit by disconnecting zero weights


\vspace{1em}

\textbf{\Large Comments from Reviewer \#3}\\

\begin{bfseries}
Review: This paper presents a framework to use sparsity to reduce the parameters and computations of a pre-trained CNN. 
The results are only reported for small datasets and networks, while it is now imperative to be able to report results on larger datasets and production-size networks.
The biggest problem with this paper is that it does not report numbers of inference time and gains, which is very important in production. And similarly for disk pace. Parameters reduction is useful only if it leads to a large decrease in space or inference time.
\end{bfseries}

We thank the reviewer for the comments.

We have tried to evaluate the proposed sparse CNN approach, extensively, which was not an easy task. The proposed scheme has been validated using a variety of network architectures and on three well-known datasets, the CIFAR-10, CIFAR-100, and SVHN datasets, under two different sparsity penalty term L0-norm and L1-norm and a variety of values assigned to the sparsity strength mu. These configurations are coherent to what many other current work on sparsifying deep neural networks are reporting. Realistically, we think that such an extensive experimental assessment can hardly be done on much more complex datasets and models in a timely manner. We prefer to be more systematic in validating the method than trying to report results on only a handful set of configurations with larger models and datasets, where we would encounter the risk of reporting only anecdotal results.

We also added more results in terms of speedup changes as we vary the parameter mu to the results of Tables 3, 4, and 5 in Appendix B. As the reviewer noticed, in practice, when the network becomes sparser with fewer parameters, its inference time is reduced. 

\textbf{\Large Comments from Reviewer \#4}\\

\begin{bfseries}
Review:The authors use the Alternating Direction Method of Multipliers (ADMM) algorithm for the first time on CNN models, allowing them to perform model compression without any appreciable loss on the CIFAR-10, CIFAR-100, and SVHN tasks.  The algorithmic details and the intuition behind their algorithm are generally well presented, (although there are occasional typos).

Pros: \\
1) Put an old algorithm to good use in a new setting\\
2) The algorithm has the nice property that it is partially analytically solvable (due to the separability property the authors mention).  This contributes to the efficient trainability of the model\\
3) Seems to dovetail nicely with other results to encourage sparsity--that is, it can be used simultaneously--and is quite generalizable.\\

Cons:\\
1) It would be nice to see a more thorough analysis of the performance gains for using this method, beyond raw data about $\%$ sparsity--some sort of comparison involving training time would be great, and is currently lacking.\\

2) I would very much like to see some discussion about why the sparsity seems to *improve* the test performance, as mentioned in my previous comment.  Is this a general feature?  Is this a statistical fluke? etc.  Even if the answer is "it is not obvious, and determining why goes outside the scope of this work", I would like to know it!

3) Based on the current text, and some of the other reviewer comments, I would appreciate an expanded discussion on how this work compares with other methods in the field.  I don't think a full numerical comparison is necessary, but some additional text discussing some of the other papers mentioned in the other reviews would greatly benefit the paper.

Additional comments: If my Cons are addressed, I would definitely raise my score to a 6 or even a 7.  The core of this paper is quite solid, it just needs a little bit more polishing. 
\end{bfseries}

Thank you very much for your comments.

[More information on training time (cons 1)]: See answers to reviewer 2.

[Why sparsity improves performance (cons 2)]: See the newly added Appendix A, which is reported statistical test values over 15 runs of the approach on NIN model on CIFAR-10. The statistical tests demonstrate that results are statistically significant. We hope these empirical results clarify the question. 

[Comparison with other methods (cons 3)]: Standard L1 regularization methods in the literature, like Structural Sparsity Learning (SSL) using group Lasso, suffer from two limitations compared to our proposed method. First, they rely on a rigid framework that avoids incorporation of non-differentiable penalty functions (e.g., not usable with L0-norm). Second, they require training the original full model for performance and sparsity with the same optimization method, while our proposed method allows to decompose the corresponding optimization problems into two sub-problems and exploit the separability of the sparsity-promoting penalty functions to find analytical solutions for one of the sub-problems, while using SGD for solving the other.

These two properties are highlighted in the statements 1, 2 at the Discussion section and the following text is also added to that section in order to clarify the distinction between our proposed method and closely related SSL approach (text beginning with "Some methods such as SSL..." up to the end of the Discussion section).

We hope the latest version of the paper will provide you with material that will convince you of the soundness of our proposal.

\end{document}
