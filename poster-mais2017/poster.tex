%!TEX encoding = UTF-8

\documentclass[final,hyperref={pdfpagelabels=false}]{beamer}

\PassOptionsToPackage{cmyk}{xcolor}

\usepackage[utf8]{inputenc}

\usepackage[orientation=landscape,size=A0,scale=1.35]{beamerposter} % ,scale=1.5

\mode<presentation>{
\usepackage{beamerthemeposterulaval}
\usefonttheme{professionalfonts}
}
\usepackage{exscale}
\pdfcompresslevel=0

\PassOptionsToPackage{cmyk}{xcolor}

\setbeamertemplate{caption}[numbered]
\setbeamertemplate{bibliography item}[text]

\usepackage{graphicx}
\usepackage{amsmath, amssymb}
\usepackage{pifont}
\usepackage{algorithm,algorithmic}
\usepackage{booktabs}
\usepackage[belowskip=5pt,aboveskip=20pt]{caption}

\graphicspath{{../figures/}{.}}

\usepackage[style=numeric-comp,sorting=none,backend=biber,natbib=true,
firstinits,url=false,doi=false,isbn=false,arxiv=false,maxcitenames=2,maxbibnames=2]{biblatex}
\bibliography{ijcnn2017.bib}
\renewcommand*{\bibfont}{\footnotesize}

\mathchardef\mhyphen="2D
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}

\newlength{\columnheight}
\setlength{\columnheight}{70cm}

\providecommand\thispdfpagelabel[1]{}

\title{ \fontsize{2.5cm}{3.75cm}\selectfont
Alternating Direction Method of Multipliers for Sparse Convolutional Neural Networks
}
\author{\texorpdfstring{Farkhondeh Kiaee \and Christian Gagné \and Mahdieh Abbasi}{}}
\institute[]{
\textsuperscript{Laboratoire de vision et systèmes numériques (LVSN), Centre de recherche sur les données massives (CRDM), Département de génie électrique et de génie informatique, Université Laval, Québec, QC, Canada}\\
\textsuperscript{Email: \texttt{christian.gagne@gel.ulaval.ca} \qquad Web: \url{http://vision.gel.ulaval.ca/~cgagne}}
}

\begin{document}
\begin{frame}{}


\begin{columns}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COLUMN 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{column}{0.32\linewidth}
\parbox[t][\columnheight]{\textwidth}{

\begin{block}{Sparse Convolutional Neural Networks (CNN)}
\begin{itemize}
\item \textbf{Motivation}: how to make deep nets sparser?
\begin{itemize}
\item More efficient use of resources
\item Run on devices with limited capabilities (e.g., embedded systems, IoT)
\end{itemize}
\item \textbf{Assumption}: sparsification can be done on trained models
\begin{itemize}
\item Allow further sparsification of the models
\item ``Legroom'' required during training may be removed after convergence
\item Sparsification done on complete convolution filters of CNNs
\end{itemize}
\end{itemize}
\begin{center}
\includegraphics[width=\linewidth]{SCNN}
\end{center}
\begin{itemize}
\item Sparse CNN objective function
\begin{equation}
\operatorname*{minimize}_{\boldsymbol{W}} ~\mathcal{L}_{net}(\boldsymbol{W}) + \mu f(\boldsymbol{W})
\label{eq:SP-objective}
\end{equation}
\begin{itemize}
\item $\boldsymbol{W}$: parameters of convolution filters
\item $\mathcal{L}_{net}(\boldsymbol{W})$: logistic loss
\item $\mu$: regularization coefficient
\item $f(\boldsymbol{W})$: penalty function on the filters
\begin{itemize}
\item Can be $l_0$-norm (cardinality of non-zero weights) or $l_1$-norm (LASSO)
\end{itemize}
\end{itemize}
\end{itemize}
\end{block}

\begin{block}{Alternating Direction Method of Multipliers (ADMM)}
\begin{itemize}
\item ADMM: a common convex optimization method (Boyd et al., 2011)
\begin{itemize}
\item \emph{Decomposition-coordination} procedure, where solutions to small local subproblems are coordinated to find the solution to a large global problem
\item Naturally parallelisable (Hadoop), useful for big data analysis
\end{itemize}
\end{itemize}

\begin{itemize}
\item Sparsifying CNN with ADMM
\begin{itemize}
\item Duplicate weights of sparse CNN objective function (\ref{eq:SP-objective}) in $\boldsymbol{W}$ and $\boldsymbol{F}$
\begin{equation}
\operatorname*{minimize}_{\boldsymbol{W},\boldsymbol{F}}~\mathcal{L}_{net}(\boldsymbol{W}) + \mu f(\boldsymbol{F}),  \quad \text{s.t.}~\boldsymbol{W}-\boldsymbol{F}=\boldsymbol{0}
\end{equation}
\item Associated augmented Lagrangian
\begin{align}
\label{eq:augmented}
\mathcal{C}(\boldsymbol{W},\boldsymbol{F},\boldsymbol{\Gamma}) & = ~\mathcal{L}_{net}(\boldsymbol{W}) + \mu f(\boldsymbol{F}) + \sum_{l,i,j}\text{trace}({\boldsymbol{\Gamma}^l_{ij}}^{T}(\boldsymbol{W}^l_{ij}-\boldsymbol{F}^l_{ij})) \nonumber\\ &\quad +\frac{\rho}{2}\sum_{l,i,j}\parallel \boldsymbol{W}^l_{ij}-\boldsymbol{F}^l_{ij}\parallel^{2}_{F}
\end{align}
\begin{itemize}
\item $\boldsymbol{\Gamma}^l_{ij}$: dual variable (i.e., Lagrange multiplier)
\item $\rho$: positive scalar
\end{itemize}
\end{itemize}
\end{itemize}
\end{block}

}
\end{column}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COLUMN 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{column}{0.32\linewidth}
\parbox[t][\columnheight]{\textwidth}{

\begin{block}{Algorithm}
While $(\parallel{\boldsymbol{F}}^{\{k+1\}} - {\boldsymbol{F}}^{\{k\}}\parallel_{F}> \epsilon)$ or $(\parallel{\boldsymbol{W}}^{\{k+1\}} - {\boldsymbol{F}}^{\{k+1\}}\parallel_{F}> \epsilon)$:
\begin{enumerate}
\item Solve \textbf{performance promoting} problem with SGD
\begin{equation}
\label{eq:PP}
{\boldsymbol{W}}^{\{k+1\}}=\operatorname*{arg\,min}_{\boldsymbol{W}} \mathcal{C}\left(\boldsymbol{W},{\boldsymbol{F}}^{\{k\}},{\boldsymbol{\Gamma}}^{\{k\}}\right)
\end{equation}
\item Solve \textbf{sparsity promoting} problem analytically
\begin{equation}
\label{eq:SP}
{\boldsymbol{F}}^{\{k+1\}} =\operatorname*{arg\,min}_{\boldsymbol{F}}\mathcal{C}\left({\boldsymbol{W}}^{\{k+1\}},\boldsymbol{F},{\boldsymbol{\Gamma}}^{\{k\}}\right )
\end{equation}
\item Update the dual variable $\boldsymbol{\Gamma}^l_{ij}$ to satisfy dual feasibility conditions
\begin{align}
\label{eq:dual}
{\boldsymbol{\Gamma}^l_{ij}}^{\{k+1\}} ={\boldsymbol{\Gamma}^l_{ij}}^{\{k\}} +\rho \left ({\boldsymbol{W}^l_{ij}}^{\{k+1\}}-{\boldsymbol{F}^l_{ij}}^{\{k+1\}}\right)
\end{align}
\end{enumerate}
\end{block}


\begin{block}{Performance promoting step}
\begin{itemize}
\item<1-> Simplify $\mathcal{C}(\boldsymbol{W},\boldsymbol{F},\boldsymbol{\Gamma})$ by completion of squares w.r.t. $\boldsymbol{W}$
\begin{equation}
\operatorname*{minimize}_{\boldsymbol{W}}~\mathcal{L}_{net}(\boldsymbol{W}) +\frac{\rho}{2}\sum_{l,i,j}\parallel \boldsymbol{W}^l_{ij}-\boldsymbol{U}^l_{ij}\parallel^2_{F}
\end{equation}
where $\boldsymbol{U}^l_{ij}= \boldsymbol{F}^l_{ij}-\frac{1}{\rho}\boldsymbol{\Gamma}^l_{ij}$
\item Sparsity penalty $\mu f(\boldsymbol{F})$ (possibly non-differentiable) excluded from this objective
\item Can be processed by automatic gradients (implemented on TensorFlow)
\end{itemize}
\end{block}

\begin{block}{Sparsity promoting step}
\begin{itemize}
\item Simplify $\mathcal{C}(\boldsymbol{W},\boldsymbol{F},\boldsymbol{\Gamma})$ by completion of squares w.r.t. $\boldsymbol{F}$
\begin{equation}
\operatorname*{minimize}_{\boldsymbol{F}}\mu f(\boldsymbol{F})+\frac{\rho}{2}\sum_{l,i,j}\parallel \boldsymbol{F}^l_{ij}-\boldsymbol{V}^l_{ij}\parallel^2_{F}
\end{equation}
where $\boldsymbol{V}^l_{ij}=\boldsymbol{W}^l_{ij}+\frac{1}{\rho}\boldsymbol{\Gamma}^l_{ij}$
\item Flexible framework to select arbitrary \textit{sparsity blocks}
\begin{itemize}
\item Both $f(\boldsymbol{F})$ (with $l_0$- or $l_1$-norm) and Frobenius norm can be written as summation of component-wise functions of a tensor
\item For our experiments, individual filter components used as the sparsity blocks
\end{itemize}
\item Solution for $l_1$-norm (soft thresholding)
\begin{align}
\label{eq:norm1}
 {\boldsymbol{F}^l_{ij}}^{*}= \left\{ 
  \begin{array}{l l}
   \left(1-\frac{\mu}{\rho\parallel\boldsymbol{V}^l_{ij}\parallel_{F}}\right)\boldsymbol{V}^l_{ij},  &\quad \text{if~}\parallel\boldsymbol{V}^l_{ij}\parallel_{F}>\frac{\mu}{\rho}\\
   0, & \quad\text{otherwise}\\
  \end{array}\right.
\end{align}
\item Solution for $l_0$-norm (hard thresholding)
\begin{align}
\label{eq:norm0}
 {\boldsymbol{F}^l_{ij}}^{*}= \left\{ 
  \begin{array}{l l}
   \boldsymbol{V}^l_{ij},  & \quad\text{if~}\parallel\boldsymbol{V}^l_{ij}\parallel_{F}>\sqrt{\frac{2\mu}{\rho}}\\
   0, & \quad\text{otherwise}\\
  \end{array}\right.
\end{align}
\end{itemize}
\end{block}


\begin{block}{Convergence properties}
\begin{itemize}
\item For convex problems, ADMM guaranteed to converge to the global optimum
\item But in deep learning, the objective function is non-convex!
\begin{itemize}
\item Sufficiently large $\rho$ tends to ``convexify'' the objective function
\item Training from scratch with ADMM does not work, but starting from pre-trained deep nets is effective
\item Allow a sparsifying fine-tuning of existing networks
\end{itemize}
\end{itemize}
\end{block}


}
\end{column}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COLUMN 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{column}{0.32\linewidth}
\parbox[t][\columnheight]{\textwidth}{

\begin{block}{Experimental setup}
\begin{itemize}
\item Tested on CIFAR-10, CIFAR-100 and SVHN for sparsifying pre-trained models
\begin{itemize}
\item Network in Network (NIN) (Lin et al., 2013) with only $3\times 3$ filters
\item Custom CNN
\item Low-rank version of both networks
\end{itemize}
\item Evaluated with $l_0$- and $l_1$-norm sparsity
\begin{itemize}
\item Low learning rate ($0.001$) to search around the current solution
\item Increasing regularization (log spaced) with an increasing number of epochs
\end{itemize}
\end{itemize}
\end{block}


\begin{block}{Results}
\includegraphics[width=\linewidth]{sparsity_plot}
\end{block}

%
%\begin{block}{References}
%\begin{itemize}
%\begin{footnotesize}
%\item Boyd, Stephen, Neal Parikh, Eric Chu, Borja Peleato, and Jonathan Eckstein. \emph{Distributed optimization and statistical learning via the alternating direction method of multipliers.} Foundations and Trends® in Machine Learning 3, no. 1 (2011). Available at \texttt{http://dx.doi.org/10.1561/2200000016}.
%\end{footnotesize}
%\end{itemize}
%\end{block}

}
\end{column}
\end{columns}

\end{frame}
\end{document}
